import { initializeApp } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyA2-O5JC0y7wIm5CE5BP-wVHoPXJuDrmRc",
    authDomain: "sevaapp-f1164.firebaseapp.com",
    projectId: "sevaapp-f1164",
    storageBucket: "sevaapp-f1164.appspot.com",
    messagingSenderId: "584343930754",
    appId: "1:584343930754:web:28ddd52eb9adb8ca8a319b",
    measurementId: "G-ZMF0EB4R57"
};


const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

function mySignup() {
    var email = document.getElementById("email").value;
    localStorage.setItem("email", email);
    var password = document.getElementById("password").value;
    createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Signed in 
            const user = userCredential.user;
            // ...
            console.log("User created successfully");
            createdSuccessfully();
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            // ..
            console.log(errorMessage);
            errorOccured();
        });
}

module.mySignup = mySignup;
module.logOut=logOut;

function createdSuccessfully() {
    var x = document.getElementById("snackbar");
            x.className = "show";
            setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}

function errorOccured() {
    var x = document.getElementById("errorOccured");
    x.className = "show";
    setTimeout(function() { x.className = x.className.replace("show", ""); }, 3000);
}

function logOut() {
    localStorage.removeItem("AccessToken");
    localStorage.clear();
    //window.location.assign("/index.html");
    var x = document.getElementById("logOut");
     x.className = "show";
     setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
     setTimeout(function () { window.location.assign("/index.html") }, 5000);
}
