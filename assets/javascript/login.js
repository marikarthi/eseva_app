import { initializeApp } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-app.js";
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-auth.js";
import { getStorage, ref, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-storage.js";
import { collection, addDoc, getFirestore } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-firestore.js";

const firebaseConfig = {
    apiKey: "AIzaSyA2-O5JC0y7wIm5CE5BP-wVHoPXJuDrmRc",
    authDomain: "sevaapp-f1164.firebaseapp.com",
    projectId: "sevaapp-f1164",
    storageBucket: "sevaapp-f1164.appspot.com",
    messagingSenderId: "584343930754",
    appId: "1:584343930754:web:28ddd52eb9adb8ca8a319b",
    measurementId: "G-ZMF0EB4R57"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
//const db = getFirestore(app)
const auth = getAuth(app);
// clicked.addEventListener("click", (e) => {
function myLogin() {
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            const user = userCredential.user;
            localStorage.setItem("email", email);
            console.log(user);
            console.log("User Verified");
            localStorage.setItem("AccessToken", user.accessToken);
            createdSuccessfull();
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            errorOccur();
        });
};

module.myLogin = myLogin;
module.logOut = logOut;

function createdSuccessfull() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
    setTimeout(function () { window.location.replace("/pages/sevadashboard.html") }, 5000);
}
function errorOccur() {
    var x = document.getElementById("errorOccur");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 2000);
}

function logOut() {
    localStorage.clear();
    var x = document.getElementById("logOut");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
    setTimeout(function () { window.location.assign("/index.html") }, 3700);
}



// -------------------------------------------------------------------------------login code completedly-------------------------------------------


const db = getFirestore(app);
const storage = getStorage();
const uploadimages = async () => {
    pleasewait();
    var imgurls = ["photo", "aadhar", "rcbook", "pan", "license", "sign", "smart"];
    var imgurl = [];
    var emailid = localStorage.getItem("email");
    for (imgurl of imgurls) {
        const storageRef = ref(storage, `${emailid}/${imgurl}`);
        console.log(imgurl);
        var file = document.getElementById(`${imgurl}`).files[0];
        uploadBytes(storageRef, file).then((snapshot) => {
            getDownloadURL(storageRef)
                .then((url) => {
                    console.log(url);
                    imgurls.push(url);
                    //createdSuccessfully();
                })
                .catch((error) => {
                    console.log(error);
                });
            console.log(snapshot);
            console.log("Uploaded a blob or file!");
        });
        details();
    }
};

module.uploadimages = uploadimages;

function pleasewait() {
    var x = document.getElementById("pleasewait");
    // console.log(x);
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 1000);
}

async function details() {
    createdSuccessfully();
    var name = document.getElementById("t1").value;
    var mobilenumber = document.getElementById("t2").value;
    var emailid = document.getElementById("email").value;
    try {
        const docRef = await addDoc(collection(db, `${emailid}`), {
            name,
            emailid,
            mobilenumber,
            photo: imageurl[0],
            aadhar: imageurl[1],
            rcbook: imageurl[2],
            pan: imageurl[3],
            license: imageurl[4],
            sign: imageurl[5],
            smart: imgurl[6],
        });
        console.log("Document written with ID: ", docRef.id);
    } catch {
        // console.log("error");
        errorOccured();
    }
    //createdSuccessfully();
}


// function pleasewait() {
//     var x = document.getElementById("pleasewait");
//     // console.log(x);
//     x.className = "show";
//     setTimeout(function () { x.className = x.className.replace("show", ""); }, 1000);
// }
function createdSuccessfully() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 2000);
    //setTimeout(function() { window.location.assign("sevadashboard.html");}, 5000);
}

function errorOccured() {
    var x = document.getElementById("errorOccured");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); },1000);
}






